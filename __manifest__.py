# -*- coding: utf-8 -*-
{
    'name': "Website Ventas Administrar Direcciones",
    'version': '17.0.1.0.0',
    'category': 'Website',
    'summary': """Permite ocultar, requerir campos Billing y Shipping""",
    'description': """Trabaja en el módulo Website""",
    'author': 'DefHelp',
    'company': 'DefHelp',
    'maintainer': 'DefHelp',
    'website': "https://www.defhelp.com",
    'depends': ['website_sale'],
    'data': [
        'views/res_config_settings_views.xml',
        'views/website_sale_templates.xml',
    ],
    'assets': {
        'web.assets_backend': [
            'website_sale_hide_manager/static/src/css/'
            'address_management.css',
        ],
    },
    'images': [
        'static/description/banner.jpg',
    ],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
